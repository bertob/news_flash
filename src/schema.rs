// @generated automatically by Diesel CLI.

diesel::table! {
    articles (article_id) {
        article_id -> Text,
        title -> Nullable<Text>,
        author -> Nullable<Text>,
        feed_id -> Text,
        url -> Nullable<Text>,
        timestamp -> Timestamp,
        synced -> Timestamp,
        html -> Nullable<Text>,
        summary -> Nullable<Text>,
        direction -> Nullable<Integer>,
        unread -> Integer,
        marked -> Integer,
        scraped_content -> Nullable<Text>,
        plain_text -> Nullable<Text>,
        thumbnail_url -> Nullable<Text>,
    }
}

diesel::table! {
    categories (category_id) {
        category_id -> Text,
        label -> Text,
    }
}

diesel::table! {
    category_mapping (parent_id, category_id) {
        parent_id -> Text,
        category_id -> Text,
        sort_index -> Nullable<Integer>,
    }
}

diesel::table! {
    enclosures (article_id) {
        article_id -> Text,
        url -> Text,
        mime_type -> Nullable<Text>,
        title -> Nullable<Text>,
    }
}

diesel::table! {
    fav_icons (feed_id) {
        feed_id -> Text,
        timestamp -> Timestamp,
        format -> Nullable<Text>,
        etag -> Nullable<Text>,
        source_url -> Nullable<Text>,
        data -> Nullable<Binary>,
    }
}

diesel::table! {
    feed_mapping (feed_id, category_id) {
        feed_id -> Text,
        category_id -> Text,
        sort_index -> Nullable<Integer>,
    }
}

diesel::table! {
    feeds (feed_id) {
        feed_id -> Text,
        label -> Text,
        website -> Nullable<Text>,
        feed_url -> Nullable<Text>,
        icon_url -> Nullable<Text>,
    }
}

diesel::table! {
    images (image_url) {
        image_url -> Text,
        article_id -> Text,
        file_path -> Text,
    }
}

diesel::table! {
    offline_actions (action_type, article_id, tag_id) {
        action_type -> Integer,
        article_id -> Text,
        tag_id -> Nullable<Text>,
    }
}

diesel::table! {
    taggings (article_id, tag_id) {
        article_id -> Text,
        tag_id -> Text,
    }
}

diesel::table! {
    tags (tag_id) {
        tag_id -> Text,
        label -> Text,
        color -> Nullable<Text>,
        sort_index -> Nullable<Integer>,
    }
}

diesel::table! {
    thumbnails (article_id) {
        article_id -> Text,
        timestamp -> Timestamp,
        format -> Nullable<Text>,
        etag -> Nullable<Text>,
        source_url -> Nullable<Text>,
        data -> Nullable<Binary>,
        width -> Nullable<Integer>,
        height -> Nullable<Integer>,
    }
}

diesel::joinable!(category_mapping -> categories (category_id));
diesel::joinable!(enclosures -> articles (article_id));
diesel::joinable!(fav_icons -> feeds (feed_id));
diesel::joinable!(feed_mapping -> feeds (feed_id));
diesel::joinable!(taggings -> articles (article_id));
diesel::joinable!(taggings -> tags (tag_id));
diesel::joinable!(thumbnails -> articles (article_id));

diesel::allow_tables_to_appear_in_same_query!(
    articles,
    categories,
    category_mapping,
    enclosures,
    fav_icons,
    feed_mapping,
    feeds,
    images,
    offline_actions,
    taggings,
    tags,
    thumbnails,
);
