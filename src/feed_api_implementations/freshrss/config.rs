use crate::feed_api::{FeedApiError, FeedApiResult};
use crate::password_encryption::PasswordEncryption;
use log::{error, info};
use serde::{Deserialize, Serialize};
use std::fs::{self, File};
use std::io::Read;
use std::path::{Path, PathBuf};

static CONFIG_NAME: &str = "freshrss.json";

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct AccountConfig {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    user_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    password: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    url: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    auth_token: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    post_token: Option<String>,
    #[serde(skip_serializing)]
    #[serde(skip_deserializing)]
    path: PathBuf,
}

impl AccountConfig {
    pub fn load(path: &Path) -> FeedApiResult<Self> {
        let path = path.join(CONFIG_NAME);
        if path.as_path().exists() {
            let mut contents = String::new();
            info!("Attempting to open config file: {:?}", path);
            let mut file = File::open(&path).map_err(move |err| {
                error!("Failed to load config file");
                FeedApiError::IO(err)
            })?;
            file.read_to_string(&mut contents).map_err(move |err| {
                error!("Reading content of file failed");
                FeedApiError::IO(err)
            })?;
            let mut config: AccountConfig = serde_json::from_str(&contents)?;

            if let Some(password) = config.password {
                let password = PasswordEncryption::decrypt(&password)?;
                config.password = Some(password);
            }

            config.path = path;

            return Ok(config);
        }

        info!("Config file does not exist. Returning empty config: {:?}", path);
        Ok(AccountConfig {
            user_name: None,
            password: None,
            auth_token: None,
            post_token: None,
            url: None,
            path,
        })
    }

    pub fn write(&self) -> FeedApiResult<()> {
        let mut config = self.clone();
        if let Some(password) = config.get_password() {
            let password = PasswordEncryption::encrypt(&password);
            config.set_password(&password);
        }
        let data = serde_json::to_string_pretty(&config)?;
        fs::write(&self.path, data).map_err(|err| {
            error!("Failed to write config to: {:?}", self.path);
            FeedApiError::IO(err)
        })?;
        Ok(())
    }

    pub fn delete(&self) -> FeedApiResult<()> {
        fs::remove_file(&self.path)?;
        Ok(())
    }

    pub fn get_user_name(&self) -> Option<String> {
        self.user_name.clone()
    }

    pub fn set_user_name(&mut self, user_name: &str) {
        self.user_name = Some(user_name.to_owned());
    }

    pub fn get_password(&self) -> Option<String> {
        self.password.clone()
    }

    pub fn set_password(&mut self, password: &str) {
        self.password = Some(password.to_owned());
    }

    pub fn get_auth_token(&self) -> Option<String> {
        self.auth_token.clone()
    }

    pub fn set_auth_token(&mut self, auth_token: &str) {
        self.auth_token = Some(auth_token.to_owned());
    }

    pub fn get_post_token(&self) -> Option<String> {
        self.post_token.clone()
    }

    pub fn set_post_token(&mut self, post_token: &str) {
        self.post_token = Some(post_token.to_owned());
    }

    pub fn get_url(&self) -> Option<String> {
        self.url.clone()
    }

    pub fn set_url(&mut self, url: &str) {
        self.url = Some(url.to_owned());
    }
}
