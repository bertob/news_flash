pub mod config;
pub mod metadata;

use self::config::AccountConfig;
use self::metadata::MinifluxMetadata;
use crate::models::{
    self, ArticleID, Category, CategoryID, CategoryMapping, DirectLogin, Enclosure, FatArticle, FavIcon, Feed, FeedID, FeedMapping, Headline,
    LoginData, PasswordLogin, PluginCapabilities, StreamConversionResult, SyncResult, TagID, TokenLogin, Url, NEWSFLASH_TOPLEVEL,
};
use crate::util;
use crate::util::favicon_cache::EXPIRES_AFTER_DAYS;
use crate::util::html2text::SUMMARY_LEN;
use crate::{
    feed_api::{FeedApi, FeedApiError, FeedApiResult, Portal},
    models::{Marked, Read},
};
use async_trait::async_trait;
use base64::engine::general_purpose::STANDARD as base64_std;
use base64::Engine;
use chrono::{DateTime, Duration, Utc};
use futures::future;
use log::{error, info, warn};
use miniflux_api::models::{Category as MinifluxCategory, Entry as MinifluxArticle, EntryStatus, Feed as MinifluxFeed, OrderBy, OrderDirection};
use miniflux_api::{ApiError as MinifluxApiError, MinifluxApi};
use reqwest::Client;
use std::collections::HashSet;
use std::convert::{From, TryInto};
use std::sync::Arc;

const DEFAULT_CATEGORY: &str = "New Category";

impl From<MinifluxApiError> for FeedApiError {
    fn from(error: MinifluxApiError) -> FeedApiError {
        match error {
            MinifluxApiError::Url(e) => FeedApiError::Url(e),
            MinifluxApiError::Json { source, json } => FeedApiError::Json { source, json },
            MinifluxApiError::Http(e) => FeedApiError::Network(e),
            MinifluxApiError::Miniflux(e) => FeedApiError::Api {
                message: format!("Miniflux Error: {}", e.error_message),
            },
            MinifluxApiError::Parse => FeedApiError::Api {
                message: MinifluxApiError::Parse.to_string(),
            },
        }
    }
}

pub struct ArticleQuery<'a> {
    pub status: Option<EntryStatus>,
    pub before: Option<i64>,
    pub after: Option<i64>,
    pub before_entry_id: Option<i64>,
    pub after_entry_id: Option<i64>,
    pub starred: Option<bool>,
    pub feed_id_set: &'a HashSet<FeedID>,
}

pub struct Miniflux {
    api: Option<MinifluxApi>,
    portal: Arc<Box<dyn Portal>>,
    logged_in: bool,
    config: AccountConfig,
}

impl Miniflux {
    fn convert_category_vec(mut categories: Vec<MinifluxCategory>) -> (Vec<Category>, Vec<CategoryMapping>) {
        categories
            .drain(..)
            .enumerate()
            .map(|(i, c)| Miniflux::convert_category(c, Some(i as i32)))
            .unzip()
    }

    fn convert_category(category: MinifluxCategory, sort_index: Option<i32>) -> (Category, CategoryMapping) {
        let MinifluxCategory { id, user_id: _, title } = category;
        let category_id = CategoryID::new(&id.to_string());

        let category = Category {
            category_id: category_id.clone(),
            label: title,
        };
        let category_mapping = CategoryMapping {
            parent_id: NEWSFLASH_TOPLEVEL.clone(),
            category_id,
            sort_index,
        };

        (category, category_mapping)
    }

    fn convert_feed(feed: MinifluxFeed) -> Feed {
        let MinifluxFeed {
            id,
            user_id: _,
            title,
            site_url,
            feed_url,
            rewrite_rules: _,
            scraper_rules: _,
            crawler: _,
            checked_at: _,
            etag_header: _,
            last_modified_header: _,
            parsing_error_count: _,
            parsing_error_message: _,
            category: _,
            icon: _,
        } = feed;

        Feed {
            feed_id: FeedID::new(&id.to_string()),
            label: title,
            website: match Url::parse(&site_url) {
                Ok(url) => Some(url),
                Err(_) => None,
            },
            feed_url: match Url::parse(&feed_url) {
                Ok(url) => Some(url),
                Err(_) => None,
            },
            icon_url: None,
        }
    }

    fn convert_feed_vec(mut feeds: Vec<MinifluxFeed>) -> (Vec<Feed>, Vec<FeedMapping>) {
        let mut mappings: Vec<FeedMapping> = Vec::new();
        let feeds = feeds
            .drain(..)
            .enumerate()
            .map(|(i, f)| {
                mappings.push(FeedMapping {
                    feed_id: FeedID::new(&f.id.to_string()),
                    category_id: CategoryID::new(&f.category.id.to_string()),
                    sort_index: Some(i as i32),
                });

                Miniflux::convert_feed(f)
            })
            .collect();

        (feeds, mappings)
    }

    fn convert_entry(entry: MinifluxArticle, portal: Arc<Box<dyn Portal>>) -> FatArticle {
        let MinifluxArticle {
            id,
            user_id: _,
            feed_id,
            title,
            url,
            comments_url: _,
            author,
            content,
            hash: _,
            published_at,
            created_at: _,
            status,
            starred,
            feed: _,
            reading_time: _,
            enclosures: _,
        } = entry;

        let article_id = ArticleID::new(&id.to_string());

        let article_exists_locally = portal.get_article_exists(&article_id).unwrap_or(false);

        let plain_text = if article_exists_locally {
            None
        } else {
            Some(nanohtml2text::html2text(&content))
        };

        let summary = plain_text.as_ref().map(|plain_text| plain_text.chars().take(SUMMARY_LEN).collect());
        let thumbnail_url = article_scraper::FullTextParser::thumbnail_from_html(&content);

        FatArticle {
            article_id,
            title: Some(title),
            author: if author.is_empty() { None } else { Some(author) },
            feed_id: FeedID::new(&feed_id.to_string()),
            url: match Url::parse(&url) {
                Ok(url) => Some(url),
                Err(_) => None,
            },
            date: match DateTime::parse_from_rfc3339(&published_at) {
                Ok(date) => date.with_timezone(&Utc),
                Err(_) => Utc::now(),
            }
            .naive_utc(),
            synced: Utc::now().naive_utc(),
            summary,
            html: Some(content),
            direction: None,
            unread: match status.as_str().try_into() {
                Ok(status) => match status {
                    EntryStatus::Read => models::Read::Read,
                    _ => models::Read::Unread,
                },
                Err(_) => models::Read::Unread,
            },
            marked: if starred { models::Marked::Marked } else { models::Marked::Unmarked },
            scraped_content: None,
            plain_text,
            thumbnail_url,
        }
    }

    async fn convert_entry_vec(entries: Vec<MinifluxArticle>, feed_ids: &HashSet<FeedID>, portal: Arc<Box<dyn Portal>>) -> StreamConversionResult {
        let tasks = entries
            .into_iter()
            .map(|e| {
                let feed_ids = feed_ids.clone();
                let portal = portal.clone();

                tokio::spawn(async move {
                    if feed_ids.contains(&FeedID::new(&e.feed_id.to_string())) || e.starred {
                        Some(Miniflux::convert_entry(e, portal))
                    } else {
                        None
                    }
                })
            })
            .collect::<Vec<_>>();

        let articles = future::join_all(tasks).await.into_iter().filter_map(|res| res.ok().flatten()).collect();

        StreamConversionResult {
            articles,
            headlines: Vec::new(),
            taggings: Vec::new(),
            enclosures: Vec::new(),
        }
    }

    pub async fn get_articles(&self, query: ArticleQuery<'_>, client: &Client) -> FeedApiResult<StreamConversionResult> {
        if let Some(api) = &self.api {
            let batch_size: i64 = 100;
            let mut offset: Option<i64> = None;
            let mut articles: Vec<FatArticle> = Vec::new();
            let mut enclosures: Vec<Enclosure> = Vec::new();

            loop {
                let entries = api
                    .get_entries(
                        query.status,
                        offset,
                        Some(batch_size),
                        Some(OrderBy::PublishedAt),
                        Some(OrderDirection::Desc),
                        query.before,
                        query.after,
                        query.before_entry_id,
                        query.after_entry_id,
                        query.starred,
                        client,
                    )
                    .await?;

                let entry_count = entries.len();
                let mut converted = Miniflux::convert_entry_vec(entries, query.feed_id_set, self.portal.clone()).await;
                articles.append(&mut converted.articles);
                enclosures.append(&mut converted.enclosures);

                if entry_count < batch_size as usize {
                    break;
                }

                offset = match offset {
                    Some(offset) => Some(offset + batch_size),
                    None => Some(batch_size),
                };
            }
            return Ok(StreamConversionResult {
                articles,
                headlines: Vec::new(),
                taggings: Vec::new(),
                enclosures,
            });
        }
        Err(FeedApiError::Login)
    }

    fn article_ids_to_i64(ids: &[ArticleID]) -> Vec<i64> {
        ids.iter().filter_map(|id| Self::article_id_to_i64(id).ok()).collect()
    }

    fn article_id_to_i64(id: &ArticleID) -> Result<i64, FeedApiError> {
        id.as_str().parse::<i64>().map_err(|_| {
            log::error!("Failed to parse ID '{}'", id);
            FeedApiError::Unknown
        })
    }

    fn feed_id_to_i64(id: &FeedID) -> Result<i64, FeedApiError> {
        id.as_str().parse::<i64>().map_err(|_| {
            log::error!("Failed to parse ID '{}'", id);
            FeedApiError::Unknown
        })
    }

    fn category_id_to_i64(id: &CategoryID) -> Result<i64, FeedApiError> {
        id.as_str().parse::<i64>().map_err(|_| {
            log::error!("Failed to parse ID '{}'", id);
            FeedApiError::Unknown
        })
    }
}

#[async_trait]
impl FeedApi for Miniflux {
    fn features(&self) -> FeedApiResult<PluginCapabilities> {
        Ok(PluginCapabilities::ADD_REMOVE_FEEDS | PluginCapabilities::SUPPORT_CATEGORIES | PluginCapabilities::MODIFY_CATEGORIES)
    }

    fn has_user_configured(&self) -> FeedApiResult<bool> {
        Ok(self.api.is_some())
    }

    async fn is_logged_in(&self, _client: &Client) -> FeedApiResult<bool> {
        Ok(self.logged_in)
    }

    async fn user_name(&self) -> Option<String> {
        self.config.get_user_name()
    }

    async fn get_login_data(&self) -> Option<LoginData> {
        if let Ok(true) = self.has_user_configured() {
            if let (Some(username), Some(password)) = (self.config.get_user_name(), self.config.get_password()) {
                return Some(LoginData::Direct(DirectLogin::Password(PasswordLogin {
                    id: MinifluxMetadata::get_id(),
                    url: self.config.get_url(),
                    user: username,
                    password,
                    basic_auth: None, // miniflux authentication already uses basic auth
                })));
            } else if let Some(token) = self.config.get_token() {
                return Some(LoginData::Direct(DirectLogin::Token(TokenLogin {
                    id: MinifluxMetadata::get_id(),
                    url: self.config.get_url(),
                    token,
                    basic_auth: None,
                })));
            }
        }

        None
    }

    async fn login(&mut self, data: LoginData, client: &Client) -> FeedApiResult<()> {
        if let LoginData::Direct(simple_login_data) = data {
            let api = match simple_login_data {
                DirectLogin::Password(password_data) => {
                    if let Some(url_string) = password_data.url.clone() {
                        self.config.set_url(&url_string);
                        self.config.set_password(&password_data.password);
                        self.config.set_user_name(&password_data.user);
                        self.config.clear_token();

                        let url = Url::parse(&url_string)?;
                        MinifluxApi::new(&url, password_data.user.clone(), password_data.password)
                    } else {
                        log::error!("No URL set");
                        return Err(FeedApiError::Login);
                    }
                }
                DirectLogin::Token(token_data) => {
                    if let Some(url_string) = token_data.url.clone() {
                        self.config.set_url(&url_string);
                        self.config.set_token(&token_data.token);
                        self.config.clear_user_name();
                        self.config.clear_password();

                        let url = Url::parse(&url_string)?;
                        MinifluxApi::new_from_token(&url, token_data.token)
                    } else {
                        log::error!("No URL set");
                        return Err(FeedApiError::Login);
                    }
                }
            };

            if self.config.get_user_name().is_none() {
                let user = api.get_current_user(client).await?;
                self.config.set_user_name(&user.username);
            }

            self.config.write()?;
            self.api = Some(api);
            self.logged_in = true;
            return Ok(());
        }

        self.logged_in = false;
        self.api = None;
        Err(FeedApiError::Login)
    }

    async fn logout(&mut self, _client: &Client) -> FeedApiResult<()> {
        self.config.delete()?;
        Ok(())
    }

    async fn initial_sync(&self, client: &Client) -> FeedApiResult<SyncResult> {
        if let Some(api) = &self.api {
            let categories = api.get_categories(client).await?;
            let (categories, category_mappings) = Miniflux::convert_category_vec(categories);

            let feeds = api.get_feeds(client).await?;
            let (feeds, feed_mappings) = Miniflux::convert_feed_vec(feeds);

            let feed_id_set: HashSet<FeedID> = feeds.iter().map(|f| f.feed_id.clone()).collect();

            let mut articles: Vec<FatArticle> = Vec::new();
            let mut enclosures: Vec<Enclosure> = Vec::new();

            // starred articles
            let query = ArticleQuery {
                status: None,
                before: None,
                after: None,
                before_entry_id: None,
                after_entry_id: None,
                starred: Some(true),
                feed_id_set: &feed_id_set,
            };
            let mut starred = self.get_articles(query, client).await?;
            articles.append(&mut starred.articles);
            enclosures.append(&mut starred.enclosures);

            // unread articles
            let query = ArticleQuery {
                status: Some(EntryStatus::Unread),
                before: None,
                after: None,
                before_entry_id: None,
                after_entry_id: None,
                starred: Some(false),
                feed_id_set: &feed_id_set,
            };
            let mut unread = self.get_articles(query, client).await?;
            articles.append(&mut unread.articles);
            enclosures.append(&mut unread.enclosures);

            // latest read articles
            let entries = api
                .get_entries(
                    Some(EntryStatus::Read),
                    None,
                    Some(100),
                    Some(OrderBy::PublishedAt),
                    Some(OrderDirection::Desc),
                    None,
                    None,
                    None,
                    None,
                    None,
                    client,
                )
                .await?;
            let mut read = Miniflux::convert_entry_vec(entries, &feed_id_set, self.portal.clone()).await;
            articles.append(&mut read.articles);
            enclosures.append(&mut read.enclosures);

            return Ok(SyncResult {
                feeds: util::vec_to_option(feeds),
                categories: util::vec_to_option(categories),
                feed_mappings: util::vec_to_option(feed_mappings),
                category_mappings: util::vec_to_option(category_mappings),
                tags: None,
                taggings: None,
                headlines: None,
                articles: util::vec_to_option(articles),
                enclosures: None,
            });
        }
        Err(FeedApiError::Login)
    }

    async fn sync(&self, max_count: u32, _last_sync: DateTime<Utc>, client: &Client) -> FeedApiResult<SyncResult> {
        if let Some(api) = &self.api {
            let categories = api.get_categories(client).await?;
            let (categories, category_mappings) = Miniflux::convert_category_vec(categories);

            let feeds = api.get_feeds(client).await?;
            let (feeds, feed_mappings) = Miniflux::convert_feed_vec(feeds);

            let feed_id_set: HashSet<FeedID> = feeds.iter().map(|f| f.feed_id.clone()).collect();

            let mut articles: Vec<FatArticle> = Vec::new();
            let mut enclosures: Vec<Enclosure> = Vec::new();
            let mut headlines: Vec<Headline> = Vec::new();

            // unread articles
            let query = ArticleQuery {
                status: Some(EntryStatus::Unread),
                before: None,
                after: None,
                before_entry_id: None,
                after_entry_id: None,
                starred: None,
                feed_id_set: &feed_id_set,
            };
            let mut unread = self.get_articles(query, client).await?;

            let remote_unread_ids: HashSet<ArticleID> = unread.articles.iter().map(|a| &a.article_id).cloned().collect();

            articles.append(&mut unread.articles);
            enclosures.append(&mut unread.enclosures);

            // marked articles
            let query = ArticleQuery {
                status: None,
                before: None,
                after: None,
                before_entry_id: None,
                after_entry_id: None,
                starred: Some(true),
                feed_id_set: &feed_id_set,
            };
            let mut marked = self.get_articles(query, client).await?;

            let remote_marked_ids: HashSet<ArticleID> = marked.articles.iter().map(|a| &a.article_id).cloned().collect();

            articles.append(&mut marked.articles);
            enclosures.append(&mut marked.enclosures);

            // get local IDs
            let local_unread_ids = self.portal.get_article_ids_unread_all()?;
            let local_marked_ids = self.portal.get_article_ids_marked_all()?;

            let local_unread_ids: HashSet<ArticleID> = local_unread_ids.into_iter().collect();
            let local_marked_ids: HashSet<ArticleID> = local_marked_ids.into_iter().collect();

            // mark remotely read article as read
            let mut should_mark_read_headlines = local_unread_ids
                .difference(&remote_unread_ids)
                .cloned()
                .map(|id| Headline {
                    article_id: ArticleID::new(&id.to_string()),
                    unread: Read::Read,
                    marked: if remote_marked_ids.contains(&id) {
                        Marked::Marked
                    } else {
                        Marked::Unmarked
                    },
                })
                .collect();
            headlines.append(&mut should_mark_read_headlines);

            // unmark remotly unstarred articles locally
            let mut missing_unmarked_headlines = local_marked_ids
                .difference(&remote_marked_ids)
                .cloned()
                .map(|id| Headline {
                    article_id: ArticleID::new(&id.to_string()),
                    marked: Marked::Unmarked,
                    unread: if remote_unread_ids.contains(&id) { Read::Unread } else { Read::Read },
                })
                .collect();
            headlines.append(&mut missing_unmarked_headlines);

            // get some recent read articles
            let entries = api
                .get_entries(
                    Some(EntryStatus::Read),
                    None,
                    Some(i64::from(max_count)),
                    Some(OrderBy::PublishedAt),
                    Some(OrderDirection::Desc),
                    None,
                    None,
                    None,
                    None,
                    None,
                    client,
                )
                .await?;
            let mut read = Miniflux::convert_entry_vec(entries, &feed_id_set, self.portal.clone()).await;

            articles.append(&mut read.articles);
            enclosures.append(&mut read.enclosures);

            return Ok(SyncResult {
                feeds: util::vec_to_option(feeds),
                categories: util::vec_to_option(categories),
                feed_mappings: util::vec_to_option(feed_mappings),
                category_mappings: util::vec_to_option(category_mappings),
                tags: None,
                taggings: None,
                headlines: Some(headlines),
                articles: util::vec_to_option(articles),
                enclosures: util::vec_to_option(enclosures),
            });
        }
        Err(FeedApiError::Login)
    }

    async fn set_article_read(&self, articles: &[ArticleID], read: models::Read, client: &Client) -> FeedApiResult<()> {
        if articles.is_empty() {
            Ok(())
        } else if let Some(api) = &self.api {
            let entries = Miniflux::article_ids_to_i64(articles);
            let status = match read {
                models::Read::Read => EntryStatus::Read,
                models::Read::Unread => EntryStatus::Unread,
            };
            api.update_entries_status(entries, status, client).await?;

            return Ok(());
        } else {
            Err(FeedApiError::Login)
        }
    }

    async fn set_article_marked(&self, articles: &[ArticleID], _marked: models::Marked, client: &Client) -> FeedApiResult<()> {
        if let Some(api) = &self.api {
            // since only the articles that need to be updated are passed to this method
            // we can ignore the "marked" parameter and simply toggle the bookmark state of all articles

            for article in articles {
                if let Ok(entry_id) = article.as_str().parse::<i64>() {
                    api.toggle_bookmark(entry_id, client).await?;
                }
            }

            return Ok(());
        }
        Err(FeedApiError::Login)
    }

    async fn set_feed_read(&self, _feeds: &[FeedID], articles: &[ArticleID], _last_sync: DateTime<Utc>, client: &Client) -> FeedApiResult<()> {
        self.set_article_read(articles, Read::Read, client).await
    }

    async fn set_category_read(
        &self,
        _categories: &[CategoryID],
        articles: &[ArticleID],
        _last_sync: DateTime<Utc>,
        client: &Client,
    ) -> FeedApiResult<()> {
        self.set_article_read(articles, Read::Read, client).await
    }

    async fn set_tag_read(&self, _tags: &[TagID], _articles: &[ArticleID], _last_sync: DateTime<Utc>, _client: &Client) -> FeedApiResult<()> {
        Err(FeedApiError::Unsupported)
    }

    async fn set_all_read(&self, articles: &[ArticleID], _last_sync: DateTime<Utc>, client: &Client) -> FeedApiResult<()> {
        self.set_article_read(articles, Read::Read, client).await
    }

    async fn add_feed(
        &self,
        url: &Url,
        title: Option<String>,
        category_id: Option<CategoryID>,
        client: &Client,
    ) -> FeedApiResult<(Feed, Option<Category>)> {
        if let Some(api) = &self.api {
            let category_id = match category_id {
                Some(category_id) => Self::category_id_to_i64(&category_id)?,
                None => {
                    info!("Creating empty category for feed");
                    match api.create_category(DEFAULT_CATEGORY, client).await {
                        Ok(category) => category.id,
                        Err(_) => {
                            warn!("Creating empty category failed");
                            info!("Checking if 'New Category' already exists");

                            let categories = api.get_categories(client).await?;

                            match categories.iter().find(|c| c.title == DEFAULT_CATEGORY) {
                                Some(new_category) => new_category.id,
                                None => match categories.first() {
                                    Some(first_category) => first_category.id,
                                    None => {
                                        let msg = "Was not able to create or find cateogry to add feed into";
                                        error!("{}", msg);
                                        return Err(FeedApiError::Api { message: msg.into() });
                                    }
                                },
                            }
                        }
                    }
                }
            };

            let feed_id = api.create_feed(url, category_id, client).await?;

            if let Some(title) = title {
                api.update_feed(feed_id, Some(&title), None, None, None, None, None, None, client).await?;
            }

            let feed = api.get_feed(feed_id, client).await?;
            let category = api
                .get_categories(client)
                .await?
                .iter()
                .find(|c| c.id == category_id)
                .map(|c| Miniflux::convert_category(c.clone(), None))
                .map(|(c, _m)| c);

            return Ok((Miniflux::convert_feed(feed), category));
        }
        Err(FeedApiError::Login)
    }

    async fn remove_feed(&self, id: &FeedID, client: &Client) -> FeedApiResult<()> {
        if let Some(api) = &self.api {
            let feed_id = Self::feed_id_to_i64(id)?;
            api.delete_feed(feed_id, client).await?;
            return Ok(());
        }
        Err(FeedApiError::Login)
    }

    async fn move_feed(&self, feed_id: &FeedID, _from: &CategoryID, to: &CategoryID, client: &Client) -> FeedApiResult<()> {
        if let Some(api) = &self.api {
            let category_id = Self::category_id_to_i64(to)?;

            let miniflux_feed_id = Self::feed_id_to_i64(feed_id)?;

            api.update_feed(miniflux_feed_id, None, Some(category_id), None, None, None, None, None, client)
                .await?;
            return Ok(());
        }
        Err(FeedApiError::Login)
    }

    async fn rename_feed(&self, feed_id: &FeedID, new_title: &str, client: &Client) -> FeedApiResult<FeedID> {
        if let Some(api) = &self.api {
            let miniflux_feed_id = Self::feed_id_to_i64(feed_id)?;

            api.update_feed(miniflux_feed_id, Some(new_title), None, None, None, None, None, None, client)
                .await?;

            return Ok(feed_id.clone());
        }
        Err(FeedApiError::Login)
    }

    async fn add_category(&self, title: &str, _parent: Option<&CategoryID>, client: &Client) -> FeedApiResult<CategoryID> {
        if let Some(api) = &self.api {
            let category = api.create_category(title, client).await?;
            return Ok(CategoryID::new(&category.id.to_string()));
        }
        Err(FeedApiError::Login)
    }

    async fn remove_category(&self, id: &CategoryID, _remove_children: bool, client: &Client) -> FeedApiResult<()> {
        if let Some(api) = &self.api {
            // FIXME: figure out how api behaves regarding deleting child feeds
            let miniflux_id = Self::category_id_to_i64(id)?;
            api.delete_category(miniflux_id, client).await?;
            return Ok(());
        }
        Err(FeedApiError::Login)
    }

    async fn rename_category(&self, id: &CategoryID, new_title: &str, client: &Client) -> FeedApiResult<CategoryID> {
        if let Some(api) = &self.api {
            let miniflux_id = Self::category_id_to_i64(id)?;
            api.update_category(miniflux_id, new_title, client).await?;
            return Ok(id.clone());
        }
        Err(FeedApiError::Login)
    }

    async fn move_category(&self, _id: &CategoryID, _parent: &CategoryID, _client: &Client) -> FeedApiResult<()> {
        Err(FeedApiError::Unsupported)
    }

    async fn import_opml(&self, opml: &str, client: &Client) -> FeedApiResult<()> {
        if let Some(api) = &self.api {
            api.import_opml(opml, client).await?;
        }
        Err(FeedApiError::Login)
    }

    async fn add_tag(&self, _title: &str, _client: &Client) -> FeedApiResult<TagID> {
        Err(FeedApiError::Unsupported)
    }

    async fn remove_tag(&self, _id: &TagID, _client: &Client) -> FeedApiResult<()> {
        Err(FeedApiError::Unsupported)
    }

    async fn rename_tag(&self, _id: &TagID, _new_title: &str, _client: &Client) -> FeedApiResult<TagID> {
        Err(FeedApiError::Unsupported)
    }

    async fn tag_article(&self, _article_id: &ArticleID, _tag_id: &TagID, _client: &Client) -> FeedApiResult<()> {
        Err(FeedApiError::Unsupported)
    }

    async fn untag_article(&self, _article_id: &ArticleID, _tag_id: &TagID, _client: &Client) -> FeedApiResult<()> {
        Err(FeedApiError::Unsupported)
    }

    async fn get_favicon(&self, feed_id: &FeedID, client: &Client) -> FeedApiResult<FavIcon> {
        if let Some(api) = &self.api {
            let miniflux_feed_id = Self::feed_id_to_i64(feed_id)?;

            let favicon = api.get_feed_icon(miniflux_feed_id, client).await?;

            if let Some(start) = favicon.data.find(',') {
                let data = base64_std.decode(&favicon.data[start + 1..]).map_err(|_| FeedApiError::Encryption)?;

                let favicon = FavIcon {
                    feed_id: feed_id.clone(),
                    expires: Utc::now().naive_utc() + Duration::days(EXPIRES_AFTER_DAYS),
                    format: Some(favicon.mime_type),
                    etag: None,
                    source_url: None,
                    data: Some(data),
                };

                return Ok(favicon);
            }
        }
        Err(FeedApiError::Login)
    }
}
