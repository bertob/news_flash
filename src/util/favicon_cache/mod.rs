mod error;
mod icon_info;
mod scraped_icon;
mod scraper;

pub use self::error::FavIconError;
use self::scraper::IconScraper;
use crate::database::Database;
use crate::feed_api::FeedApi;
use crate::models::{FavIcon, Feed, FeedID, Url};
use chrono::{DateTime, Duration, Utc};
use image::codecs::png::PngEncoder;
use image::io::Reader;
use image::{imageops, GenericImageView, ImageEncoder};
use log::{error, info, warn};
use moka::future::Cache;
use reqwest::{self, Client};
use std::io::Cursor;
use std::sync::Arc;
use tokio::sync::RwLock;

pub const PREFERED_SIZE: u32 = 32;
pub const MAX_SIZE: u32 = 512;
pub const EXPIRES_AFTER_DAYS: i64 = 30;
pub const CACHE_SIZE: u64 = 99999;

pub struct FavIconCache {
    cache: Cache<FeedID, FavIcon>,
    db: Arc<Database>,
}

impl FavIconCache {
    pub fn new(db: &Arc<Database>) -> Result<Self, FavIconError> {
        let cache = Cache::new(CACHE_SIZE);

        // fill cache with icons from db
        if let Ok(favicons) = db.read_favicons() {
            for favicon in favicons {
                cache.blocking().insert(favicon.feed_id.clone(), favicon);
            }
        }

        Ok(FavIconCache { cache, db: db.clone() })
    }

    pub async fn get_icon(&self, feed_id: &FeedID, api: &RwLock<Box<dyn FeedApi>>, client: &Client) -> Result<FavIcon, FavIconError> {
        let favicon = self.cache.get(feed_id);

        if let Some(favicon) = favicon {
            // if expired: remove from cache and move on
            if favicon.is_expired() {
                info!("Favicon for '{}' is expired", feed_id);
                self.cache.remove(feed_id).await;
            } else {
                return Ok(favicon);
            }
        }

        // Check db for existing icon
        let favicon = self.db.read_favicon(feed_id).ok();
        let valid_icon = favicon.clone().filter(|favicon| favicon.data.is_some() && !favicon.is_expired());

        if let Some(favicon) = valid_icon {
            self.cache.insert(feed_id.clone(), favicon.clone()).await;
            Ok(favicon)
        } else {
            let future = Self::get_and_write_new_icon(&self.db, api, feed_id, favicon.as_ref(), client);
            let favicon = self.cache.try_get_with_by_ref(feed_id, future).await.map_err(|error| {
                error!("Failed to get icon {error}");
                FavIconError::NoFeed
            })?;
            Ok(favicon)
        }
    }

    async fn get_and_write_new_icon(
        db: &Arc<Database>,
        api: &RwLock<Box<dyn FeedApi>>,
        feed_id: &FeedID,
        old_icon: Option<&FavIcon>,
        client: &Client,
    ) -> Result<FavIcon, FavIconError> {
        info!("Downloading new icon for '{:?}'", feed_id);

        // try to download new icon
        let feeds = db.read_feeds()?;
        let feed = feeds.iter().find(|f| &f.feed_id == feed_id).ok_or(FavIconError::NoFeed)?;
        let favicon = Self::fetch_new_icon(Some(api), feed, client, old_icon, None).await;
        let favicon = if let Ok(resized_icon) = Self::resize_icon(favicon.clone()) {
            resized_icon
        } else {
            favicon
        };

        // write new icon to db
        db.insert_favicon(&favicon).map_err(|e| {
            error!("Failed to write favicon to db '{:?}'", feed_id);
            FavIconError::DB(e)
        })?;

        Ok(favicon)
    }

    fn resize_icon(original: FavIcon) -> Result<FavIcon, FavIconError> {
        let data = original.data.ok_or(FavIconError::Resize)?;
        let image = Reader::new(Cursor::new(data))
            .with_guessed_format()
            .map_err(|_| FavIconError::Resize)?
            .decode()
            .map_err(|_| FavIconError::Resize)?;

        let (original_width, original_height) = image.dimensions();
        let (dest_width, dest_height) = Self::calc_favicon_dimensions(original_width, original_height);

        let resized = imageops::resize(&image, dest_width, dest_height, imageops::FilterType::Triangle);
        let (width, height) = resized.dimensions();

        let resized_raw = resized.into_vec();

        let mut dest = Cursor::new(Vec::new());
        let encoder = PngEncoder::new(&mut dest);
        encoder
            .write_image(&resized_raw, width, height, image::ColorType::Rgba8)
            .map_err(|_| FavIconError::Resize)?;

        Ok(FavIcon {
            feed_id: original.feed_id,
            expires: original.expires,
            format: Some("image/png".into()),
            etag: original.etag,
            source_url: original.source_url,
            data: Some(dest.into_inner()),
        })
    }

    fn calc_favicon_dimensions(original_width: u32, original_height: u32) -> (u32, u32) {
        if original_width <= PREFERED_SIZE && original_height <= PREFERED_SIZE {
            return (original_width, original_height);
        }

        let ratio = (original_width as f64) / (original_height as f64);
        if original_width >= original_height {
            (PREFERED_SIZE, (PREFERED_SIZE as f64 / ratio) as u32)
        } else {
            ((PREFERED_SIZE as f64 * ratio) as u32, PREFERED_SIZE)
        }
    }

    pub async fn fetch_new_icon(
        api: Option<&RwLock<Box<dyn FeedApi>>>,
        feed: &Feed,
        client: &Client,
        old_icon: Option<&FavIcon>,
        prefered_size: Option<u32>,
    ) -> FavIcon {
        if let Some(api) = api {
            if let Ok(favicon) = api.read().await.get_favicon(&feed.feed_id, client).await {
                if let (Some(source_url), None) = (&favicon.source_url, &favicon.data) {
                    if let Ok(favicon) = Self::download(source_url, &feed.feed_id, client, old_icon).await {
                        info!("Favicon downloaded backend source url");
                        if favicon.data.as_deref().map(Self::check_aspect_ratio).unwrap_or(false) {
                            return favicon;
                        }
                    }
                } else if favicon.data.is_some() {
                    info!("Favicon downloaded from backend.");
                    return favicon;
                } else {
                    warn!("Favicon from backend doesn't contain data or source url");
                }
            }
        }

        if let Some(icon_url) = &feed.icon_url {
            if let Ok(favicon) = Self::download(icon_url, &feed.feed_id, client, old_icon).await {
                info!("Favicon downloaded from feed data.");
                if favicon.data.as_deref().map(Self::check_aspect_ratio).unwrap_or(false) {
                    return favicon;
                }
            }
        }

        if let Some(favicon) = Self::scrap(feed, client, prefered_size).await {
            info!("Favicon scraped from website.");
            return favicon;
        }

        // everything failed -> return empty icon
        FavIcon {
            feed_id: feed.feed_id.clone(),
            expires: Self::gen_expires().naive_utc(),
            format: None,
            etag: None,
            source_url: None,
            data: None,
        }
    }

    fn check_aspect_ratio(data: &[u8]) -> bool {
        if let Some(image) = Reader::new(Cursor::new(data))
            .with_guessed_format()
            .ok()
            .and_then(|image| image.decode().ok())
        {
            let aspect_ratio = image.width() as f32 / image.height() as f32;
            if aspect_ratio > 1.5 {
                log::debug!("Image is very wide with an apect ratio of {aspect_ratio}. It is probably not a favicon");
                false
            } else {
                true
            }
        } else {
            false
        }
    }

    async fn scrap(feed: &Feed, client: &Client, prefered_size: Option<u32>) -> Option<FavIcon> {
        if let Some(website) = &feed.website {
            if let Ok(scraper) = IconScraper::from_http(website, client, prefered_size).await {
                if let Some(scraped_icon) = scraper.fetch_best(client).await {
                    info!("Scraped favicon from url: '{}'", scraped_icon.info.url);
                    return Some(FavIcon {
                        feed_id: feed.feed_id.clone(),
                        expires: Self::gen_expires().naive_utc(),
                        format: scraped_icon.mime,
                        etag: scraped_icon.etag,
                        source_url: Some(Url::new(scraped_icon.info.url)),
                        data: Some(scraped_icon.data),
                    });
                } else {
                    warn!("Failed to download best scrapped icon from: '{}'", website);
                }
            } else {
                warn!("Failed to scrap icon from: {}", website);
            }
        }

        None
    }

    async fn download(url: &Url, feed_id: &FeedID, client: &Client, old_icon: Option<&FavIcon>) -> Result<FavIcon, FavIconError> {
        let res = client.get(url.as_str()).send().await?;
        let etag = res
            .headers()
            .get(reqwest::header::ETAG)
            .and_then(|etag| etag.to_str().ok())
            .map(ToString::to_string);
        let content_type = res
            .headers()
            .get(reqwest::header::CONTENT_TYPE)
            .and_then(|etag| etag.to_str().ok())
            .map(ToString::to_string);

        if let Some(old_icon) = old_icon {
            if let Some(old_etag) = old_icon.etag.as_deref() {
                if let Some(http_etag) = etag.as_deref() {
                    if old_etag == http_etag {
                        return Ok(old_icon.clone());
                    }
                }
            }
        }

        let data = res.bytes().await?.to_vec();

        Ok(FavIcon {
            feed_id: feed_id.clone(),
            expires: Self::gen_expires().naive_utc(),
            format: content_type,
            etag,
            source_url: Some(url.clone()),
            data: Some(data),
        })
    }

    fn gen_expires() -> DateTime<Utc> {
        Utc::now() + Duration::days(EXPIRES_AFTER_DAYS)
    }
}

#[cfg(test)]
mod tests {

    use super::FavIconCache;
    use crate::models::{Feed, FeedID, Url};
    use crate::util::feed_parser::{self, ParsedUrl};
    use reqwest::{Client, ClientBuilder};

    async fn prepare_feed(url_str: &str) -> (Client, Feed) {
        let client = Client::new();
        let url = Url::parse(url_str).unwrap();
        let feed_id = FeedID::new(url_str);
        let feed = feed_parser::download_and_parse_feed(&url, &feed_id, None, &client).await.unwrap();

        let feed = match feed {
            ParsedUrl::SingleFeed(feed) => *feed,
            ParsedUrl::MultipleFeeds(_) => panic!("Expected Single Feed"),
        };

        (client, feed)
    }

    #[tokio::test(flavor = "current_thread")]
    pub async fn golem() {
        let (client, golem_feed) = prepare_feed("https://rss.golem.de/rss.php?feed=ATOM1.0").await;
        let favicon = FavIconCache::scrap(&golem_feed, &client, None).await.unwrap();

        assert_eq!(favicon.feed_id, golem_feed.feed_id);
        assert!(favicon.format.expect("No favicon format").starts_with("image/"));
    }

    #[tokio::test(flavor = "current_thread")]
    pub async fn planet_gnome() {
        let (client, gnome_feed) = prepare_feed("http://planet.gnome.org/rss20.xml").await;
        let favicon = FavIconCache::scrap(&gnome_feed, &client, None).await.unwrap();

        assert_eq!(favicon.feed_id, gnome_feed.feed_id);
        assert!(favicon.format.expect("No favicon format").starts_with("image/"));
    }

    #[tokio::test(flavor = "current_thread")]
    pub async fn reddit_scraper() {
        let reddit_feed = Feed {
            feed_id: FeedID::new("http://reddit.com"),
            label: String::from("reddit"),
            website: Some(Url::parse("http://reddit.com").unwrap()),
            feed_url: None,
            icon_url: None,
        };

        // reddit will deny requests without user agent
        let client = ClientBuilder::new()
            .user_agent("Mozilla/5.0 (X11; Linux x86_64; rv:109.0)")
            .build()
            .unwrap();

        let favicon = FavIconCache::scrap(&reddit_feed, &client, None).await.unwrap();

        assert_eq!(favicon.feed_id, reddit_feed.feed_id);
        assert!(favicon.format.expect("No favicon format").starts_with("image/"));
    }

    #[tokio::test(flavor = "current_thread")]
    pub async fn golem_scraper() {
        let reddit_feed = Feed {
            feed_id: FeedID::new("http://golem.de"),
            label: String::from("Golem"),
            website: Some(Url::parse("http://golem.de").unwrap()),
            feed_url: None,
            icon_url: None,
        };
        let client = Client::new();

        let favicon = FavIconCache::scrap(&reddit_feed, &client, None).await.unwrap();

        assert_eq!(favicon.feed_id, reddit_feed.feed_id);
        assert!(favicon.format.expect("No favicon format").starts_with("image/"));
    }

    #[tokio::test(flavor = "current_thread")]
    pub async fn serienjunkies_scraper() {
        let feed = Feed {
            feed_id: FeedID::new("https://www.serienjunkies.de/news/"),
            label: String::from("Serienjunkies"),
            website: Some(Url::parse("https://www.serienjunkies.de/news/").unwrap()),
            feed_url: None,
            icon_url: None,
        };
        let client = ClientBuilder::new().user_agent("Wget/1.20.3 (linux-gnu)").build().unwrap();

        let favicon = FavIconCache::scrap(&feed, &client, None).await.unwrap();

        assert_eq!(favicon.feed_id, feed.feed_id);
        assert!(favicon.data.is_some());
    }

    #[tokio::test(flavor = "current_thread")]
    pub async fn spiegel_scraper() {
        let feed = Feed {
            feed_id: FeedID::new("http://www.spiegel.de/"),
            label: String::from("Serienjunkies"),
            website: Some(Url::parse("http://www.spiegel.de/").unwrap()),
            feed_url: None,
            icon_url: None,
        };
        let client = ClientBuilder::new().user_agent("Wget/1.20.3 (linux-gnu)").build().unwrap();

        let favicon = FavIconCache::scrap(&feed, &client, None).await.unwrap();

        assert_eq!(favicon.feed_id, feed.feed_id);
        assert!(favicon.data.is_some());
    }
}
