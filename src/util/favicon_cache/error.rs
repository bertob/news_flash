use crate::database::DatabaseError;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum FavIconError {
    #[error("Error performing DB operation")]
    DB(#[from] DatabaseError),
    #[error("Http request failed")]
    Http(#[from] reqwest::Error),
    #[error("Error parsing HTML")]
    Html,
    #[error("No Feed")]
    NoFeed,
    #[error("Error resizing icon")]
    Resize,
}
