use crate::models::{FeedID, Url};
use crate::schema::feeds;
use feed_rs::model::Feed as FeedRS;
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

#[derive(Identifiable, Clone, Insertable, Queryable, Eq, Debug)]
#[diesel(primary_key(feed_id))]
#[diesel(table_name = feeds)]
pub struct Feed {
    pub feed_id: FeedID,
    pub label: String,
    pub website: Option<Url>,
    pub feed_url: Option<Url>,
    pub icon_url: Option<Url>,
}

impl Hash for Feed {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.feed_id.hash(state);
    }
}

impl PartialEq for Feed {
    fn eq(&self, other: &Feed) -> bool {
        self.feed_id == other.feed_id
    }
}

impl Feed {
    pub fn from_feed_rs(feed: FeedRS, title: Option<String>, url: &Url) -> Self {
        let title = match title {
            Some(title) => title,
            None => match feed.title {
                Some(title) => title.content,
                None => "Unknown Feed".into(),
            },
        };

        // see if there is a link with rel='alternate' -> this is probably what we want
        let website = feed
            .links
            .iter()
            .find(|link| link.rel == Some("alternate".to_owned()))
            .and_then(|link| Url::parse(&link.href).ok());
        // otherwise just take the first link
        let website = match website {
            Some(website) => Some(website),
            None => feed.links.first().and_then(|link| Url::parse(&link.href).ok()),
        };

        let icon_url = match feed.icon.and_then(|icon| Url::parse(&icon.uri).ok()) {
            Some(url) => Some(url),
            None => feed.logo.and_then(|logo| Url::parse(&logo.uri).ok()),
        };

        // if the feed itself doesn't contain any useful way to generate an ID hash the url instead
        let url_string = url.to_string();
        let feed_id = if !feed.links.iter().any(|link| link.href == url_string) {
            let mut hasher = DefaultHasher::new();
            for byte in url_string.as_bytes() {
                hasher.write_u8(*byte);
            }
            let url_hash = format!("{:x}", hasher.finish());
            FeedID::new(&url_hash)
        } else {
            FeedID::new(&feed.id)
        };

        Feed {
            feed_id,
            label: title,
            website,
            feed_url: Some(url.clone()),
            icon_url,
        }
    }
}

//------------------------------------------------------------------

#[derive(Queryable, Debug)]
pub struct FeedCount {
    pub feed_id: FeedID,
    pub count: i64,
}
