use crate::models::{LoginGUI, PluginID, Url};

#[derive(Debug)]
pub struct PluginInfo {
    pub id: PluginID,
    pub name: String,
    pub icon: Option<PluginIcon>,
    pub icon_symbolic: Option<PluginIcon>,
    pub website: Option<Url>,
    pub service_type: ServiceType,
    pub license_type: ServiceLicense,
    pub service_price: ServicePrice,
    pub login_gui: LoginGUI,
}

#[derive(Debug, Clone)]
pub enum PluginIcon {
    Vector(VectorIcon),
    Pixel(PixelIcon),
}

impl PluginIcon {
    pub fn into_data(self) -> Vec<u8> {
        match self {
            Self::Vector(icon) => icon.data,
            Self::Pixel(icon) => icon.data,
        }
    }
}

#[derive(Debug, Clone)]
pub struct VectorIcon {
    pub data: Vec<u8>,
    pub width: i32,
    pub height: i32,
}

#[derive(Debug, Clone)]
pub struct PixelIcon {
    pub data: Vec<u8>,
    pub width: i32,
    pub height: i32,
    pub has_alpha: bool,
    pub bits_per_sample: i32,
    pub row_stride: i32,
}

#[derive(Debug, Clone)]
pub enum ServiceType {
    Local,
    Remote { self_hosted: bool },
}

#[derive(Debug, Clone)]
pub enum ServiceLicense {
    ApacheV2,
    AGPLv3,
    GPLv2,
    GPLv3,
    MIT,
    LGPLv21,
    GenericFree,
    GenericProprietary,
}

#[derive(Debug, Clone)]
pub enum ServicePrice {
    Free,
    Paid,
    PaidPremimum,
}
