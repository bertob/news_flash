use crate::models::CategoryID;
use crate::schema::categories;
use std::hash::{Hash, Hasher};

#[derive(Identifiable, Clone, Insertable, Queryable, Eq, Debug)]
#[diesel(primary_key(category_id))]
#[diesel(table_name = categories)]
pub struct Category {
    pub category_id: CategoryID,
    pub label: String,
}

impl Hash for Category {
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.category_id.hash(hasher);
    }
}

impl PartialEq for Category {
    fn eq(&self, other: &Category) -> bool {
        self.category_id == other.category_id
    }
}
