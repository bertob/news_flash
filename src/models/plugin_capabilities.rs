use bitflags::bitflags;

bitflags! {
    #[derive(Clone, Copy, Default, Debug)]
    pub struct PluginCapabilities: u32 {
        const NONE                  = 0b0000_0000;
        const ADD_REMOVE_FEEDS      = 0b0000_0001;
        const SUPPORT_CATEGORIES    = 0b0000_0010;
        const MODIFY_CATEGORIES     = 0b0000_0100;
        const SUPPORT_TAGS          = 0b0000_1000;
        const SUPPORT_SUBCATEGORIES = 0b0001_0000;
    }
}

impl PluginCapabilities {
    pub fn support_mutation(&self) -> bool {
        self.contains(PluginCapabilities::ADD_REMOVE_FEEDS) && self.contains(PluginCapabilities::MODIFY_CATEGORIES)
    }
}
