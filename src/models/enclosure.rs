use crate::models::ArticleID;
use crate::models::Url;
use crate::schema::enclosures;

#[derive(Identifiable, Queryable, Eq, PartialEq, Debug, Insertable, Clone)]
#[diesel(primary_key(article_id))]
#[diesel(table_name = enclosures)]
pub struct Enclosure {
    pub article_id: ArticleID,
    pub url: Url,
    pub mime_type: Option<String>,
    pub title: Option<String>,
}
