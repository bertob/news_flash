ALTER TABLE articles ADD thumbnail_url TEXT default null;

CREATE TABLE thumbnails (
	article_id TEXT NOT NULL REFERENCES articles(article_id),
    timestamp DATETIME NOT NULL,
	format TEXT,
	etag TEXT,
	source_url TEXT,
	data BLOB,
	width INTEGER,
	height INTEGER,
    PRIMARY KEY (article_id)
);

DROP TRIGGER on_delete_article_trigger;

CREATE TRIGGER on_delete_article_trigger
	AFTER DELETE ON articles
	BEGIN
		DELETE FROM taggings WHERE taggings.article_id=OLD.article_id;
		DELETE FROM enclosures WHERE enclosures.article_id=OLD.article_id;
        DELETE FROM thumbnails WHERE thumbnails.article_id=OLD.article_id;
	END;